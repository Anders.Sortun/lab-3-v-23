package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        String outputStr = "";
        for (String segment : args){
            outputStr += segment + " ";
        }
        return outputStr;
    }

    @Override
    public String getName() {
        return "echo";
    }

}